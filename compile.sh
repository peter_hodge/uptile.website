#!/bin/bash

fail() {
    echo "ERROR: $@" >&2
    exit 1
}

if ! which realpath &> /dev/null; then
    which readlink > /dev/null || fail "realpath and readlink utilities not available"
    realpath() { readlink -f "$1"; }
fi
which dirname  > /dev/null || fail "dirname utility not available"

CMD=$0
REAL_CMD="$(realpath $CMD)"
CMD_BASE="$(dirname "$REAL_CMD")"


# mandatory argument #1
MOSAIC_PATH="$1"
test -z "$MOSAIC_PATH" && fail "USAGE: $0 MOSAIC_PATH"
test -d "$MOSAIC_PATH" || fail "Invalid MOSAIC_PATH: $MOSAIC_PATH"
shift

# optional argument #1 - root
if [ -z "$1" ]; then
    ROOTPATH=/
else
    ROOTPATH="$1"
    shift
fi

# optional argument #2 - $SRC
if [ -z "$1" ]; then
    SRC="$CMD_BASE/src"
else
    SRC="$1"
    shift
fi

# optional argument #3 - $WWW
if [ -z "$1" ]; then
    WWW="$CMD_BASE/www"
else
    WWW="$1"
    shift
fi

rm -rf "$WWW"
mkdir -p "$WWW"
WWW="$(realpath "$WWW")"

# first up - copy all .html and .css and .js files straight across
OLD_PWD="$PWD"

if [ -s "$SRC/.htaccess" ]; then
    cp -f "$SRC/.htaccess" "$WWW/.htaccess"
fi
cd $SRC
for file in $(find * -name "*.css" -o -name "*.html" -o -name "*.js"); do
    F_DIR="$(dirname "$file")"
    mkdir -p "$WWW/$F_DIR"
    php -d display_errors=0 -d log_errors=1 -d error_log= "$file" "$MOSAIC_PATH" "$ROOTPATH" > "$WWW/$file" || fail "Could not compile $WWW/$file"
done
cd "$OLD_PWD"
chmod -R 755 "$WWW"
chmod 644 $(find "$WWW" \! -type d)

