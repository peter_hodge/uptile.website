<?php

class Tile_RefMenu implements Mosaic\Tile {
    use Mosaic\Traits\Tile;

    private $__selected; // does this owrk?

    private $__sections;

    private $__indexfor;
    private $__index = array();

    function addMethodIndex($indexfor, $anchors) {
        assert(is_string($indexfor));
        assert(in_array($indexfor, array('Tile', 'Tile_Control', 'Tile_Form')));
        $this->__indexfor = $indexfor;
        $this->__index = $anchors;
    }

    function tileGetHTML() {
        $html  = "<div {$this->tileGetAttrs()}>";
        $html .= '<strong>Mosaic\\Tile</strong>';
        $html .= '<a href="/ref/tile/index">Usage Guide</a>';
        $html .= '<a href="/ref/tile/methods">Method Reference</a>';
        if ($this->__indexfor === 'Tile')
            foreach ($this->__index as $anchor)
                $html .= "<a class=\"__index\" href=\"#$anchor\">$anchor</a>";
        $html .= '<strong>Mosaic\\Tile_Control</strong>';
        $html .= '<a href="/ref/tile_control/index">Usage Guide</a>';
        $html .= '<a href="/ref/tile_control/methods">Method Reference</a>';
        if ($this->__indexfor === 'Tile_Control')
            foreach ($this->__index as $anchor)
                $html .= "<a class=\"__index\" href=\"#$anchor\">$anchor</a>";
        $html .= '<strong>Mosaic\\Tile_Form</strong>';
        $html .= '<a href="/ref/tile_form/index">Usage Guide</a>';
        $html .= '<a href="/ref/tile_form/methods">Method Reference</a>';
        if ($this->__indexfor === 'Tile_Form')
            foreach ($this->__index as $anchor)
                $html .= "<a class=\"__index\" href=\"#$anchor\">$anchor</a>";
        $html .= '<strong>Best Practices</strong>';
        $html .= '<a href="/guidelines/css">CSS Guidelines</a>';
        $html .= '<a href="/guidelines/js">Javascript Guidelines</a>';
        $html .= "</div>";
        return $html;
    }
}

