<?php

class Tile_MenuBar implements Mosaic\Tile {
    use Mosaic\Traits\Tile;

    private $__menu = array();
    private $__selected;

    function __construct() {
        $this->__menu['home']      = array("Home",            "/");
        $this->__menu['download']  = array("Download",        "/download");
        $this->__menu['tutorial']  = array("Getting Started", "/tutorial");
        $this->__menu['reference'] = array("Documentation",   "/ref/");
    }

    function selectMenuItem($key) {
        if (isset($this->__menu[$key]))
            $this->__selected = $key;
    }

    public function tileGetHTML() {
        $html = "<div {$this->tileGetAttrs()}>";
        foreach ($this->__menu as $key => $info) {
            list($title, $path) = $info;
            $titleHTML = html($title);
            if ($this->__selected === $key)
                $titleHTML = "<b>$titleHTML</b>";
            $path = rtrim(ROOTPATH, '/').$path;
            $html .= "<a href=\"$path\">$titleHTML</a>";
        }
        $html .= '</div>';
        return $html;
    }
}

