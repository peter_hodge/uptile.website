<?php

class Tile_Page implements Mosaic\Tile {
    use Mosaic\Traits\Tile;

    private $__title;
    private $__menu;
    private $__ref_menu = false;

    private $__css     = array();
    private $__scripts = array();

    function __construct() {
        $this->__menu = new Tile_MenuBar;
    }

    function addCSS($href) {
        $this->__css[] = $href;
    }

    function addScript($href) {
        $this->__scripts[] = $href;
    }

    public function setTitle($title) {
        // TODO - this should be in use on *every* page
        $this->__title = $title;
    }

    public function selectMenuItem($key) {
        $this->__menu->selectMenuItem($key);
    }

    private $__submenu_title;
    private $__submenu_items = array();

    public function addSubMenu($title, $items) {
        assert(is_string($title) && strlen($title));
        assert(is_array($items) && count($items));
        $this->__submenu_title = $title;
        $this->__submenu_items = $items;
    }

    private $__content;

    public function setContent($HTML) {
        $this->__content = $HTML;
    }

    public function showRefMenu($show_ref_menu) {
        $this->__ref_menu = (bool)$show_ref_menu;
    }

    public function tileGetHTML() {
        $head = "<title>TODO - proper title</title>\n";
        foreach ($this->__css as $href)
            $head .= "<link rel=\"stylesheet\" href=\"$href\"/>\n";
        foreach ($this->__scripts as $src)
            $head .= "<script type=\"text/javascript\" src=\"$src\"></script>\n";
        $head = "<head>\n$head</head>\n";

        // show the references menu?
        $ref_menu = null;
        if ($this->__ref_menu) {
            $ref_menu = new Tile_RefMenu;
            $ref_menu->tileAddClass('__sidemenu');
            if ($this->__submenu_items)
                $ref_menu->addMethodIndex($this->__submenu_title, $this->__submenu_items);
        }

        $wildcards = array(
            '%%TITLE%%' => 'Mosaic for PHP',
        );
        $contentHTML = str_replace(array_keys($wildcards), array_values($wildcards), $this->__content);

        // replace links in content
        $base = rtrim(ROOTPATH, '/');
        $contentHTML = preg_replace('/\[([^\|\]]+?)\|([^\]]+?)\]/', "<a href=\"$base\$1\">\$2</a>", $contentHTML);

        $body = "<body>\n"
              . $this->__menu->tileGetHTML()
              . '<div class="__notmenu">'
              . ($ref_menu ? $ref_menu->tileGetHTML() : "")
              . ($this->__title ? '<h1 class="__pagetitle">'.html($this->__title).'</h1>' : "")
              . '<div class="__pagecontent">'.$contentHTML.'</div>'
              . '</div>' // .__notmenu
              . "</body>";

        $html = "<!DOCTYPE html>\n"
              . "<html class=\"Tile_Page\">\n"
              . $head
              . $body
              . "</html>\n";
        return $html;
    }
}
