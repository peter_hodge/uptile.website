<?php

ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('assert.warn', true);
ini_set('assert.bail', true);

define("MOSAIC_PATH", $argv[1]);
require_once MOSAIC_PATH."/php/Mosaic.php";

define("ROOTPATH", $argv[2]);

// tile loader
$LOADER = new Mosaic\Loader;
$LOADER->loaderAddBase(true);
$LOADER->loaderAddPath(dirname(__DIR__));
$LOADER->loaderLoadTile('Tile_Page');
$LOADER->loaderLoadTile('Tile_MenuBar');
$LOADER->loaderLoadTile('Tile_RefMenu');

function html($text) {
    return htmlentities($text);
}

global $PAGE;
$PAGE = new Tile_Page;
$PAGE->addCSS(ROOTPATH."style.css");
$PAGE->addScript(ROOTPATH."pretty-much-everything.js");


